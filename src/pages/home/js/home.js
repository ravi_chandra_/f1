(function (angular) {
    var moduleName = 'champions.home';
    var componentName = 'championsHome';
    var templateUrl = 'pages/home/html/home.html';

    function ChampionsHomeController(championsService) {
        var $ctrl = this;
        championsService.getChampions().then(function (champions) {
            $ctrl.champions = champions.MRData.StandingsTable.StandingsLists;
        }, function (error) {
            $ctrl.champions = error.data;
        });
    }
    ChampionsHomeController.$inject = [
        'championsService'
    ];

    function ChampionsService($http, $q) {

        function getChampions () {
            return $http
                .get('http://ergast.com/api/f1/driverStandings/1.json?offset=55')
                .then(function (res) {
                    if (res.data) {
                        return res.data;
                    }
                    return $q.reject(res);
                });
        }

        return {
            getChampions: getChampions
        };
    }

    ChampionsService.$inject = [
        '$http',
        '$q'
    ];

    angular.module(moduleName, [
        templateUrl
    ])
    .value('$routerRootComponent', 'championsHome')
    .component(componentName, {
        templateUrl: templateUrl,
        controller: ChampionsHomeController
    })
    .service('championsService', ChampionsService);
})(angular);
