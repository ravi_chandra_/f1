describe('home', function () {
    'use strict';

    var championsService;
    var element;
    var $scope;
    var $q;

    beforeEach(module('champions.home'));

    beforeEach(inject(function ($rootScope, $compile, _$q_, _championsService_) {
        $scope = $rootScope.$new();
        $q = _$q_;
        championsService = _championsService_;
        element = $compile(angular.element('<champions-home></champions-home>'))($scope);
    }));

    it('should format champions list', function () {
        var mockData = {
            MRData: {
                xmlns: 'http://ergast.com/mrd/1.4',
                series: 'f1',
                url: 'http://ergast.com/api/f1/driverstandings/1.json',
                limit: '30',
                offset: '55',
                total: '66',
                StandingsTable: {
                    driverStandings: '1',
                    StandingsLists: [
                      {
                        season: '2005',
                        round: '19',
                        DriverStandings: [
                          {
                            position: '1',
                            positionText: '1',
                            points: '133',
                            wins: '7',
                            Driver: {
                                driverId: 'alonso',
                                permanentNumber: '14',
                                code: 'ALO',
                                url: 'http://en.wikipedia.org/wiki/Fernando_Alonso',
                                givenName: 'Fernando',
                                familyName: 'Alonso',
                                dateOfBirth: '1981-07-29',
                                nationality: 'Spanish'
                            },
                            Constructors: [
                              {
                                constructorId: 'renault',
                                url: 'http://en.wikipedia.org/wiki/Renault_F1',
                                name: 'Renault',
                                nationality: 'French'
                            }
                            ]
                        }
                        ]
                    }
                    ]
                }
            }
        };
        spyOn(championsService, 'getChampions').and.returnValue($q.when(mockData));
        $scope.$digest();
        var ctrl = element.isolateScope().$ctrl;

        expect(championsService.getChampions).toHaveBeenCalled();
        expect(ctrl.champions).toEqual(mockData.MRData.StandingsTable.StandingsLists);
    });
});
