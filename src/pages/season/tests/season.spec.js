describe('season', function () {
    'use strict';

    var seasonService;
    var $scope;
    var $q;
    var element;

    beforeEach(module('champions.season'));

    beforeEach(inject(function ($rootScope, $compile, _$q_, _seasonService_) {
        $scope = $rootScope.$new();
        $q = _$q_;
        seasonService = _seasonService_;
        element = $compile(angular.element('<season-detail></season-detail>'))($scope);
    }));

    it('should format driver seasons', function () {
        var mockData = {
            MRData: {
                xmlns: 'http://ergast.com/mrd/1.4',
                series: 'f1',
                url: 'http://ergast.com/api/f1/2015/results/1.json',
                limit: '30',
                offset: '0',
                total: '19',
                RaceTable: {
                    season: '2015',
                    position: '1',
                    Races: [
                      {
                        season: '2015',
                        round: '1',
                        url: 'http://en.wikipedia.org/wiki/2015_Australian_Grand_Prix',
                        raceName: 'Australian Grand Prix',
                        Circuit: {
                            circuitId: 'albert_park',
                            url: 'templateUrl',
                            circuitName: 'Albert Park Grand Prix Circuit',
                            Location: {
                                lat: '-37.8497',
                                long: '144.968',
                                locality: 'Melbourne',
                                country: 'Australia'
                            }
                        },
                        date: '2015-03-15',
                        time: '05:00:00Z',
                        Results: [
                          {
                            number: '44',
                            position: '1',
                            positionText: '1',
                            points: '25',
                            Driver: {
                                driverId: 'hamilton',
                                permanentNumber: '44',
                                code: 'HAM',
                                url: 'templateUrl',
                                givenName: 'Lewis',
                                familyName: 'Hamilton',
                                dateOfBirth: '1985-01-07',
                                nationality: 'British'
                            },
                            Constructor: {
                                constructorId: 'mercedes',
                                url: 'templateUrl',
                                name: 'Mercedes',
                                nationality: 'German'
                            },
                            grid: '1',
                            laps: '58',
                            status: 'Finished',
                            Time: {
                                millis: '5514067',
                                time: '1:31:54.067'
                            },
                            FastestLap: {
                                rank: '1',
                                lap: '50',
                                Time: {
                                    time: '1:30.945'
                                },
                                AverageSpeed: {
                                    units: 'kph',
                                    speed: '209.915'
                                }
                            }
                        }
                        ]
                    }
                    ]
                }
            }
        };
        spyOn(seasonService, 'getSeasons').and.returnValue($q.when(mockData));

        $scope.$digest();
        var ctrl = element.isolateScope().$ctrl;
    });
});
