# **F1 world champions** #
## Steps to run the application: ##

1. Install npm dependencies

    ```
    npm install (or) sudo npm install
    ```

2. Update bower dependencies

    ```
    bower update
    ```

3. Run the build

    ```
    grunt
    ```

4. Open website in url : http://localhost:8000/champions/

Note : Softwares required - NodeJs, ruby(for scss compilation)

### Updating the theme ###

1. open src/scss/_variables.scss file

2. Change the $teal variable to the selected theme basic colour(exp: $teal: #00bcd4)

## Application Architecture: ##
### Frameworks used ###

1. Angularjs
   Version - 1.5.8
   Used for JavaScript MVC
   https://angularjs.org/

2. Component Router:
   Have used angular component router rather angular router to get angularJs 2 component structure
   https://docs.angularjs.org/guide/component-router

3. Syntactically Awesome Style Sheets (SCSS):
   Used to get benefits of Object oriented programing in css3
   http://sass-lang.com/

4. Grunt : Task runner, for automation of project build
   http://gruntjs.com/

5. I have used Bower and npm as package managers, here its bad practise but in given time i was not able to avoid.
   https://www.npmjs.com/ and https://bower.io/

6. Code validation tools: jscsrc, jshint

7. Jasmine : unit testing framework
   http://jasmine.github.io/

8. Karma : Test runner
   https://karma-runner.github.io

### Folder structure ###

![Screen Shot 2016-10-14 at 17.27.27.png](https://bitbucket.org/repo/XqKMqq/images/1264797265-Screen%20Shot%202016-10-14%20at%2017.27.27.png)

### Unit testing ###

**Executing unit test cases**
    ```
    grunt karma:unit
    ```

For continuous feedback look, unit tests are configured with grunt watch task, which helps to give feedback on every time when code changes
![Screen Shot 2016-10-14 at 23.22.24.png](https://bitbucket.org/repo/XqKMqq/images/3895606582-Screen%20Shot%202016-10-14%20at%2023.22.24.png)

### E2E testing ###

**Executing E2E test cases**
    ```
    grunt e2e
    ```

Only configured, no sample e2e test cases available in the source

##Application screenshot##

### F1 world champions ###
![home.png](https://bitbucket.org/repo/XqKMqq/images/1590347743-home.png)

### List of the winners for every race ###
![seasons.png](https://bitbucket.org/repo/XqKMqq/images/3254383892-seasons.png)